/* Submit the MTU for a local address to a remote address */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <arpa/inet.h>


int main (int argc, char *argv []) {
	char *subst_argv [3];
	if (argc != 3) {
		fprintf (stderr, "Usage: %s local6addr remote6addr\n", *argv);
		exit (1);
	}
	int errors = 0;
	//
	// Initialise the local socket address
	struct sockaddr_in6 l6sa;
	memset (&l6sa, 0, sizeof (l6sa));
	l6sa.sin6_family = AF_INET6;
	if (inet_pton (AF_INET6, argv [1], &l6sa.sin6_addr) != 1) {
		fprintf (stderr, "%s: Failed to parse \"%s\" as an IPv6 address\n", *argv, argv [1]);
		errors++;
		goto fail;
	}
	//
	//
	// Initialise the remote socket address, using the discard/9 protocol
	struct sockaddr_in6 r6sa;
	memset (&r6sa, 0, sizeof (r6sa));
	r6sa.sin6_family = AF_INET6;
	r6sa.sin6_port = htons (9);
	if (inet_pton (AF_INET6, argv [2], &r6sa.sin6_addr) != 1) {
		fprintf (stderr, "%s: Failed to parse \"%s\" as an IPv6 address\n", *argv, argv [2]);
		errors++;
		goto fail;
	}
	//
	// Construct a socket
	int sox = socket (AF_INET6, SOCK_DGRAM, 0);
	if (sox < 0) {
		perror ("Failed to allocate a socket");
		errors++;
		goto fail;
	}
	//
	// Bind to the specified local address
	if (bind (sox, (struct sockaddr *) &l6sa, sizeof (l6sa)) != 0) {
		perror ("Failed to bind to local address");
		errors++;
		goto sox_fail;
	}
	listen (sox, 10);
	//
	// Connect to the specified remote address
	if (connect (sox, (struct sockaddr *) &r6sa, sizeof (r6sa)) != 0) {
		perror ("Failed to connect to remote address");
		errors++;
		goto sox_fail;
	}
	//
	// Fetch the MTU -- SOL_SOCKET or IPPROTO_IPV6
	int mtu = 0;
	socklen_t mtusz = sizeof (mtu);
	if (getsockopt (sox, IPPROTO_IPV6, IPV6_MTU, (void *) &mtu, &mtusz) != 0) {
		perror ("Failed to retrieve socket MTU");
		errors++;
		goto sox_fail;
	}
	//
	// Report what we found
	printf ("mtu=%d for local=%s, remote=%s\n", mtu, argv [1], argv [2]);
	//
	// Now send UDP messages (which add 40 + 8 bytes) up to the IPV6_MTU
	// Messages contain the byte code of how much less, so '0' is just right
	if (mtu > 65536) {
		mtu = 65536;
	}
	char whole [65536];
	for (int less = 0; less < 127; less++) {
		memset (whole, less, mtu-less);
		putchar ((char) ((less >= ' ') ? less : 'X'));
		fflush (stdout);
		ssize_t sent = send (sox, whole, mtu-less, 0);
		if (sent < 0) {
			perror ("\nError sending UDP message");
			errors++;
		} else if (sent != mtu-less) {
			fprintf (stderr, "\n%s: Only %zd out of %d was sent\n", *argv, sent, mtu-less);
			errors++;
		}
	}
	putchar ('\n');
	fflush (stdout);
	//
	// Close the socket
sox_fail:
	close (sox);
fail:
	if (errors > 0) {
		fprintf (stderr, "%s: Exit after %d errors\n", *argv, errors);
		exit (1);
	} else {
		exit (0);
	}
}
