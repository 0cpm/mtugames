# Experiment with Linux' Virtual Ether Pairs

> *Linux networks offer `veth` pairs that work like a wire.
> The MTU can be set differently on each end.  Does one side's MTU
> function as the other side's MRU?*

Let's setup a couple of network namespaces and interfaces to play with.

## Setup

Network namespace `lomtu` has an interface `lo0` with MTU 1280
coupled to `hi0` in the main network namespace.

```
ip netns add lomtu
ip link add hi0 type veth peer name lo0 netns lomtu
ip -n lomtu link set lo0 mtu 1280
ip -n lomtu addr add fc12:345:67::80/64 dev lo0
ip addr add fc12:345:67::89/64 dev hi0
ip link set up hi0
ip -n lomtu link set up lo0
```

Check the high end with `ip -6 addr show dev hi0` to find

```
4: hi0@if2: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000 link-netns lomtu
    inet6 fc12:345:67::89/64 scope global 
       valid_lft forever preferred_lft forever
    inet6 fe80::8aa:aaff:fec4:ae27/64 scope link 
       valid_lft forever preferred_lft forever
```


Check the low end with `ip -6 -n lomtu addr show dev lo0` to find

```
2: lo0@if4: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1280 qdisc noqueue state UP group default qlen 1000 link-netnsid 0
    inet6 fc12:345:67::80/64 scope global 
       valid_lft forever preferred_lft forever
    inet6 fe80::f005:99ff:fea1:44c8/64 scope link 
       valid_lft forever preferred_lft forever
```

## Measurements

First, sending down from high-MTU to low-MTU,

```
ip netns exec lomtu tshark -S0 -w /tmp/lo0dn.pcap -ni lo0 &
tshark -S0 -w /tmp/hi0dn.pcap -ni hi0
submit-mtu fc12:345:67::89 fc12:345:67::80
```

This yields `hi0dn.pcap` with 200676 bytes and `lo0dn.pcap` with 7392 bytes.


Second, sending up from low-MTU to high-MTU,

```
ip netns exec lomtu tshark -S0 -w /tmp/lo0up.pcap -ni lo0 &
tshark -S0 -w /tmp/hi0up.pcap -ni hi0
sudo ip netns exec lomtu submit-mtu fc12:345:67::80 fc12:345:67::89
```

The last command gives error output, which reflects ICMPv6 feedback in packet traces:

```
XX
Error sending UDP message: Connection refused
XX
Error sending UDP message: Connection refused
XX
Error sending UDP message: Connection refused
XX
Error sending UDP message: Connection refused
XX
Error sending UDP message: Connection refused
XX
Error sending UDP message: Connection refused
build/submit-mtu: Exit after 6 errors
```

The test run yields `hi0up.pcap` with 172092 bytes and `lo0up.pcap` with 172092 bytes.


## Analysis

 1. Sending down, from high-MTU to low-MTU, only fragments arrive.
    The fragments that arrive are the smaller ones with the "tail" of the UDP frame.
    The MTU-sized frame as dimensioned for the high-MTU side does not arrive.
 2. Sending down, all of the non-fragmented frames exceed the low MTU, so they do not arrive.
 3. Sending down, there is no ICMPv6 feedback about Unreachable Port/Service.
 4. Sending down, there is no ICMPv6 feedback about Packet Too Big.
 5. Sending up, from low-MTU to high-MTU, the first 6 packets receive ICMPv6 feedback Unreachable Port/Service.
 6. Sending up, the first 6 packets all send even content (huh?) 00, 02, 04, 06, 08, 0a, 0c.  Beyond that, 0d, 0e, 0f, ...
 7. Sending up, fragmentation stops where expected; frame with content 30, ethernet size 1294 = MTU 1280 + ether header 14.
 8. Sending up, everything seems to arrive; the ICMPv6 feedback is not shown


## Conclusions

 1. It is *not true* that `veth` applies the one side's MTU as the other side's MRU
 2. The MTU and MRU seem to match on both ends of a `veth` connector

The hope was to construct a different MRU and MTU in the operating system.  This would
have helped to send smaller frames without constraining the size that others may send.

**It is an OS design flaw that MRU is set to the same value as MTU.**
This only works when the entire Internet uses the same MTU everywhere.

**MTU variation is devastating for unconnected IPv6 and for unconnected IPv4 flagged Don't Fragment.**
Traffic is dropped, and feedback over ICMPv6 is not processed by the socket.

**Remediation is required in the application.**
There are a few ways, and they are all a bit of a nuisance.

  * Use `IPV6_RECVERR` or `IP_RECVERR` socket options, and receive ICMPv6/ICMP feedback
    with `recvmsg (..., MSG_ERRQUEUE)` to learn about traffic sent at a higher MTU than
    desired.
  * When these events happen, immediate resending is permissible; waiting for a timeout
    would introduce a risk of hitting too many MTU bumps.  Note that a limitation on the
    amount of resends exists; do not resend without [considerably] lowering the MTU.
  * Worth testing: Open multiple sockets with `SO_REUSEADDR` or `SO_REUSEPORT` but set
    different MTUs.  Fall back to a secondary socket with a [consderably] lower MTU for
    sending, but receive on the primary one (with a free/maximal socket MTU).

**The best fix is still at the OS level.**
Allow differentiation between MTU and MRU, both for interfaces and sockets.
*Be conservative in what you send, and liberal in what you accept.*

