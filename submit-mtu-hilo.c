/* Submit high and low MTU for a local address to an unconnected remote address */

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <arpa/inet.h>




/**********   THIS ATTEMPT FAILED YET BROUGHT NEW INSIGHTS   **********/

/*
 * Confusingly, ip(7) states
 *
 * IP_MTU (since Linux 2.2)
 *    Retrieve the current known path MTU of the current socket.
 *    Returns an integer.  IP_MTU  is valid only for getsockopt(2) and
 *    can be employed only when the socket has been connected.
 *
 * Similarly, ipv6(7) states
 *
 * IPV6_MTU
 *     getsockopt(): Retrieve the current known path MTU of the current
 *     socket.  Valid only when the socket has been connected.  Returns
 *     an integer.
 *     
 *     setsockopt():  Set  the  MTU to be used for the socket.  The MTU
 *     is limited by the device MTU or the path MTU when path MTU
 *     discovery is enabled.  Argument is a pointer to integer.
 *
 * This suggests that IP_MTU is a socket property.  However, it makes
 * more sense as a shared global property, which indeed seems to apply:
 *
 * The ipv6(7) entry for IPV6_MTU_DISCOVER references IP_MTU_DISCOVER;
 * the ip(7) entry for IP_MTU_DISCOVER states
 *
 * IP_MTU_DISCOVER (since Linux 2.2)
 *    When PMTU discovery is enabled, the kernel automatically keeps track
 *    of the path MTU  per destination host.  When it is connected to a
 *    specific peer with connect(2), the currently known path MTU can be
 *    retrieved conveniently using the IP_MTU socket option (e.g.,  after
 *    an  EMSGSIZE  error  occurred).   The  path MTU may change over time.
 *    For connectionless sockets with many destinations, the new MTU for a
 *    given destination can also be  accessed using  the  error  queue (see
 *    IP_RECVERR).  A new error will be queued for every incoming MTU update.
 *
 *    While MTU discovery is in progress, initial packets from datagram
 *    sockets may be dropped.  Applications  using  UDP  should  be aware
 *    of this and not take it into account for their packet retransmit strategy.
 *
 * Retransmission is common in UDP applications.  Ideally, the IP_RECVERR or
 * IPV6_RECVERR are used to immediately resend, without wait for timers to
 * expire; and without limiting the number of Path MTU lessens learnt to the
 * number of timer rounds.
 *
 * For IPv6, where fragmenttion is required to accomodate the Path MTU, and
 * for unconnected applications, the lessons from Path MTU discovery are of
 * major impact on their behaviour; we should always let the socket fragment
 * frames when so desired, so:
 *
 * IP_MTU_DISCOVER (since Linux 2.2)
 *    IP_PMTUDISC_WANT will fragment a datagram if needed according to the
 *    path MTU, [IPv4-only: or will set the don't-fragment flag otherwise].
 *
 *    Path MTU discovery value   Meaning
 *    IP_PMTUDISC_WANT           Use per-route settings.
 *    IP_PMTUDISC_DONT           Never do Path MTU Discovery.
 *    IP_PMTUDISC_DO             Always do Path MTU Discovery.
 *    IP_PMTUDISC_PROBE          Set DF but ignore Path MTU.
 *
 */



// Linux and BSD have SO_REUSEPORT to better control additional binds
// than SO_REUSEADDR, so that is preferred
//
#ifdef REUSE_PORT
# define SO_REUSE_VARIANT SO_REUSEPORT
#else
# define SO_REUSE_VARIANT SO_REUSEADDR
#endif


static int errors = 0;


int make_socket (struct sockaddr_in6 *l6sa) {
	//
	// Construct a socket
	int sox = socket (AF_INET6, SOCK_DGRAM, 0);
	if (sox < 0) {
		perror ("Failed to allocate a socket");
		goto fail;
	}
	//
	// Reuse the adddress or port so we can bind() twice
	// and assign different MTU to the sockets
	int one = 1;
	socklen_t onesz = sizeof (one);
	if (setsockopt (sox, SOL_SOCKET, SO_REUSE_VARIANT, (void *) &one, onesz) != 0) {
		perror ("Failed to configure socket MTU");
		goto sox_fail;
	}
	//
	// Bind to the specified local address
	if (bind (sox, (struct sockaddr *) l6sa, sizeof (*l6sa)) != 0) {
		perror ("Failed to bind to local address");
		goto sox_fail;
	}
	listen (sox, 10);
	//
	// Return the socket -- success
	return sox;
	//
	// Close the socket and return failure
sox_fail:
	close (sox);
	//
	// Report failure
fail:
	errors++;
	return -1;
}


bool report_mtu (int sox, int *mtu) {
	//
	// We cannot connect twice to the specified remote address
	//
	// Fetch the MTU -- SOL_SOCKET or IPPROTO_IPV6
	socklen_t mtusz = sizeof (mtu);
	if (getsockopt (sox, IPPROTO_IPV6, IPV6_MTU, (void *) mtu, &mtusz) != 0) {
		perror ("Failed to retrieve socket MTU");
		errors++;
		return false;
	}
	//
	// Report what we found
	printf ("Fetch mtu=%d on socket %d\n", *mtu, sox);
	return true;
}


bool config_mtu (int sox, int mtu) {
	//
	// Report what we want to do
	printf ("Write mtu=%d to socket %d\n", mtu, sox);
	//
	// Fetch the MTU -- SOL_SOCKET or IPPROTO_IPV6
	socklen_t mtusz = sizeof (mtu);
	if (setsockopt (sox, IPPROTO_IPV6, IPV6_MTU, (void *) &mtu, mtusz) != 0) {
		perror ("Failed to configure socket MTU");
		errors++;
		return false;
	}
	return true;
}


int main (int argc, char *argv []) {
	bool ok = true;
	//
	// Parse arguments
	char *subst_argv [3];
	if (argc != 3) {
		fprintf (stderr, "Usage: %s local6addr remote6addr\n", *argv);
		exit (1);
	}
	//
	// Initialise the local socket address
	struct sockaddr_in6 l6sa;
	memset (&l6sa, 0, sizeof (l6sa));
	l6sa.sin6_family = AF_INET6;
	if (inet_pton (AF_INET6, argv [1], &l6sa.sin6_addr) != 1) {
		fprintf (stderr, "%s: Failed to parse \"%s\" as an IPv6 address\n", *argv, argv [1]);
		errors++;
		goto fail;
	}
	//
	//
	// Initialise the remote socket address, using the discard/9 protocol
	struct sockaddr_in6 r6sa;
	memset (&r6sa, 0, sizeof (r6sa));
	r6sa.sin6_family = AF_INET6;
	r6sa.sin6_port = htons (9);
	if (inet_pton (AF_INET6, argv [2], &r6sa.sin6_addr) != 1) {
		fprintf (stderr, "%s: Failed to parse \"%s\" as an IPv6 address\n", *argv, argv [2]);
		errors++;
		goto fail;
	}
	//
	// Grab two sockets
	int sox [2];
	sox [0] = make_socket (&l6sa);
	sox [1] = make_socket (&l6sa);
	if ((sox [0] < 0) || (sox [1] < 0)) {
		goto sox_fail;
	}
	printf ("Allocated sockets %d and %d\n", sox [0], sox [1]);
	//
	// Set a lower MTU on the second socket
	int mtu [2] = { 1400, 1400+'0'-'A' };
	for (int soxi = 0; soxi < 2; soxi++) {
		ok = config_mtu (sox [soxi], mtu [soxi]) && ok;
	}
	//
	// Report the MTU on both sockets
	for (int soxi = 0; soxi < 2; soxi++) {
		ok = report_mtu (sox [soxi], &mtu [soxi]) && ok;
	}
	//
	// The rest of the tests makes no sense if MTU setup failed
	if (!ok) {
		goto sox_fail;
	}
	//
	// Print expectations -- normally '0' and 'A' as largest messages
	for (int soxi = 0; soxi < 2; soxi++) {
		printf ("On socket %d, expect the largest message to contain '%c'\n", 
				sox [soxi], mtu [0] - mtu [soxi] + '0');
	}
	//
	// Now send UDP messages (which add 40 + 8 bytes) up to the IPV6_MTU
	// Messages contain the byte code of how much less, so '0' is just right
	// for mtu[0], whereas 'A' should be just right for mtu [1].
	char whole [65536];
	for (int less = 0; less < 127; less++) {
		memset (whole, less, mtu[0]-less);
		putchar ((char) ((less >= ' ') ? less : 'X'));
		fflush (stdout);
		for (int soxi = 0; soxi < 2; soxi++) {
			whole [0] = '0' + sox [soxi];
			//
			// We cannot connect twice to the specified remote address
			// so we use sendto() in this variant of the program
			ssize_t sent = sendto (sox [0], whole, mtu[0]-less, 0,
						(struct sockaddr *) &r6sa, sizeof (r6sa));
			if (sent < 0) {
				perror ("\nError sending UDP message");
				errors++;
			} else if (sent != mtu[0]-less) {
				fprintf (stderr, "\n%s: Only %zd out of %d was sent\n", *argv, sent, mtu[0]-less);
				errors++;
			}
		}
	}
	putchar ('\n');
	fflush (stdout);
	//
	// Close the sockets
sox_fail:
	for (int soxi = 0; soxi < 2; soxi++) {
		if (sox [soxi] >= 0) {
			close (sox [soxi]);
		}
	}
fail:
	if (errors > 0) {
		fprintf (stderr, "%s: Exit after %d errors\n", *argv, errors);
		exit (1);
	} else {
		exit (0);
	}
}
