# MTU Games

> *A network with an MTU below 1500 can run into problems.  Tests, code.*

Most IPv4 networks maintain an MTU of 1500, based on the ethernet MTU.
For IPv6, delivery is still done over tunnels, which causes the MTU
for IPv6 to be lower on such endpoints.

IPv6 has no fragmentation for network traffic in transit.  Instead, it
refuses to relay to a network with a lower MTU and sends back an ICMPv6
message Packet Too Big.

Connected sockets will learn from this ICMPv6 feedback, and tone down
the Path MTU used for that connection.  Resends then allow for fast
recovery.  Unconnected sockets (notably UDP) are not so lucky, they
drop the ICMPv6 feedback by default and effectively drop traffic.

**SIP Traffic** is a typical use case for IPv6 where this is a problem.
See this
[explanation for Kamailio](https://github.com/kamailio/kamailio/issues/3119)
for an i-depth example.


## Central Problem

**MTU can differ between IPv4 and IPv6**
yet network interfaces configure the same value for both.

**MTU is not MRU**
because you may be willing to receive larger frames than you
intend to send.  Network interfaces that conflate MTU and MRU
make it impossible to setup connectionless UDP systems that are
*conservative in what they send, and liberal in what they receive*.
(This is a central adagio for network protocols, it is a popular
phrase by Jon Postel.)


## Work-arounds

**Interfaces.**
One option could be ethernet interface pairs, like `veth` on Linux.
These are used to bring a network connection into a container, and
being able to set its MTU internally to a lower value than the MRU
*could work*.

*See veth for the failing measurements on Linux 5.13.0.*

**Sockets.**
It is possible to process ICMPv6 Packet Too Big feedback, but only
when a socket can differentiate between MRU and MTU.  This *could
work* when the same socket is opened more than once, and set to
different MTU and MRU values.  Since sockets are numeric references
to kernel data structures, this is not likely to work after a
`fork()` or `clone()` of a socket, but it *could work* to use a
facility like `REUSE_ADDR` or `REUSE_PORT` and receive from only
one with high MTU while optionally sending over ones with lower
MTU.

